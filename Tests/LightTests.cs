﻿namespace Tests;

using System.Numerics;

public class LightTests
{
    private Scene mockScene;
    private Vector3 testPoint;
    private Vector3 testView;
    private Vector3 testNormalP1;
    private Vector3 testNormalM1;
    private Sphere testSphere;

    [SetUp]
    public void SetUp()
    {
        mockScene = Substitute.For<Scene>();
        testSphere = new Sphere
        {
            Center = new Vector3(0, -1, 3),
            Radius = 1,
            Color = new Vector3(255, 0, 0),
            Specular = 500,
        };
        testPoint = new Vector3
        {
            X = 10.0f,
            Y = 20.0f,
            Z = 30.0f,
        };
        testNormalP1 = new Vector3
        {
            X = 1.0f,
            Y = 1.0f,
            Z = 1.0f,
        };
        testNormalM1 = new Vector3
        {
            X = -1.0f,
        };
        testView = new Vector3
        {
            X = 100.0f,
            Y = 200.0f,
            Z = 300.0f,
        };
    }

    [Test]
    [TestCase(0.0f)]
    [TestCase(0.5f)]
    [TestCase(1.0f)]
    [TestCase(-1.0f)]
    [TestCase(99.0f)]
    public void TestAmbientLight(float intensity)
    {
        var light = new AmbientLight
        {
            Intensity = intensity,
        };

        Assert.That(light.Compute(mockScene, testPoint, testNormalM1, testView, -1), Is.EqualTo(intensity));
    }

    [Test]
    public void TestPointLightShadowCastingDefault()
    {
        mockScene.ClosestIntersection(Arg.Any<Vector3>(), Arg.Any<Vector3>(), Arg.Any<double>(), Arg.Any<double>())
            .Returns((testSphere, 1.0d));

        var light = new PointLight
        {
            Intensity = 1.0f,
        };

        Assert.That(light.Compute(mockScene, testPoint, testNormalM1, testView, -1), Is.EqualTo(0.0f));
    }

    [Test]
    public void TestPointLightShadowCastingSwitchedOn()
    {
        mockScene.ClosestIntersection(Arg.Any<Vector3>(), Arg.Any<Vector3>(), Arg.Any<double>(), Arg.Any<double>())
            .Returns((testSphere, 1.0d));

        var light = new PointLight
        {
            Intensity = 1.0f,
            CastShadows = true,
        };

        Assert.That(light.Compute(mockScene, testPoint, testNormalM1, testView, -1), Is.EqualTo(0.0f));
    }

    [Test]
    public void TestPointLightShadowCastingSwitchedOff()
    {
        mockScene.ClosestIntersection(Arg.Any<Vector3>(), Arg.Any<Vector3>(), Arg.Any<double>(), Arg.Any<double>())
            .Returns((testSphere, 1.0d));

        var light = new PointLight
        {
            Intensity = 1.0f,
            CastShadows = false,
        };

        Assert.That(light.Compute(mockScene, testPoint, testNormalM1, testView, -1), Is.InRange(0.26f, 0.27f));
    }

    [Test]
    public void TestPointLightSpecular()
    {
        mockScene.ClosestIntersection(Arg.Any<Vector3>(), Arg.Any<Vector3>(), Arg.Any<double>(), Arg.Any<double>())
            .Returns((testSphere, 1.0d));

        var light = new PointLight
        {
            Intensity = 1.0f,
            CastShadows = false,
        };

        Assert.That(light.Compute(mockScene, testPoint, testNormalM1, testView, 1), Is.InRange(1.124f, 1.125f));
    }

    [Test]
    public void TestDirectionalLightShadowCastingDefault()
    {
        mockScene.ClosestIntersection(Arg.Any<Vector3>(), Arg.Any<Vector3>(), Arg.Any<double>(), Arg.Any<double>())
            .Returns((testSphere, 1.0d));

        var light = new DirectionalLight
        {
            Intensity = 1.0f,
        };

        Assert.That(light.Compute(mockScene, testPoint, testNormalP1, testView, -1), Is.EqualTo(0.0f));
    }

    [Test]
    public void TestDirectionalLightShadowCastingSwitchedOn()
    {
        mockScene.ClosestIntersection(Arg.Any<Vector3>(), Arg.Any<Vector3>(), Arg.Any<double>(), Arg.Any<double>())
            .Returns((testSphere, 1.0d));

        var light = new DirectionalLight
        {
            Intensity = 1.0f,
            CastShadows = true,
        };

        Assert.That(light.Compute(mockScene, testPoint, testNormalP1, testView, -1), Is.EqualTo(0.0f));
    }
}

