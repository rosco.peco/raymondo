﻿using System.Numerics;

namespace Raymondo
{
    public class Scene
    {
        public List<IShape> Shapes { get; set; }
        public List<ILight> Lights { get; set; }

        public Scene()
        {
            Shapes = new List<IShape> { };
            Lights = new List<ILight> { };
        }

        public virtual (IShape?, double) ClosestIntersection(Vector3 origin, Vector3 viewCoord, double t_min, double t_max)
        {
            var closest_t = double.PositiveInfinity;
            IShape? closest_sphere = null;


            foreach (var shape in Shapes)
            {
                var (t1, t2) = shape.IntersectRay(origin, viewCoord);

                if ((t_min <= t1 && t1 <= t_max) && t1 < closest_t)
                {
                    closest_t = t1;
                    closest_sphere = shape;
                }

                if ((t_min <= t2 && t2 <= t_max) && t2 < closest_t)
                {
                    closest_t = t2;
                    closest_sphere = shape;
                }
            }

            return (closest_sphere, closest_t);
        }
    }
}

