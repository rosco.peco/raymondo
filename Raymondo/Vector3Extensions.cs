﻿using System.Numerics;

using BigGustave;

namespace Raymondo
{
    public static class Vector3Extensions
    {
        /**
         * Convert a Vector3 containing R, G and B values (in X, Y and Z 
         * respectively) to a PngBuilder Pixel. 
         * 
         * All values will be clamped to 255 to prevent wraparound to black.
         */
        public static Pixel ToPixel(this Vector3 vector3)
        {
            return new Pixel(
                (byte)Math.Min(vector3.X, 255),
                (byte)Math.Min(vector3.Y, 255),
                (byte)Math.Min(vector3.Z, 255)
            );
        }
    }
}

