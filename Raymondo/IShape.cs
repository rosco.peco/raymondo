﻿using System.Numerics;

namespace Raymondo
{
    public interface IShape
    {
        public Vector3 Center { get; set; }
        public Vector3 Color { get; set; }
        public float Specular { get; set; }
        public float Reflectivity { get; set; }

        public (double, double) IntersectRay(Vector3 origin, Vector3 viewCoord);
        public (Vector3, Vector3) ComputePointNormal(Vector3 origin, Vector3 viewCoord, double closest_t);
    }
}

