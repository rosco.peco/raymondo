﻿using System.Numerics;

namespace Raymondo
{
    public class Sphere : IShape
    {
        public Vector3 Center { get; set; }
        public double Radius { get; set; }
        public Vector3 Color { get; set; }
        public float Specular { get; set; }
        public float Reflectivity { get; set; }

        public Sphere()
        {
            Center = new Vector3(0, 0, 0);
            Color = new Vector3(255, 255, 255);
        }

        public (double, double) IntersectRay(Vector3 origin, Vector3 viewCoord)
        {
            var r = Radius;
            var co = origin - Center;

            var a = Vector3.Dot(viewCoord, viewCoord);
            var b = 2 * Vector3.Dot(co, viewCoord);
            var c = Vector3.Dot(co, co) - r * r;

            var discriminant = b * b - 4 * a * c;
            if (discriminant < 0)
            {
                return (double.PositiveInfinity, double.PositiveInfinity);
            }

            return ((-b + Math.Sqrt(discriminant)) / (2 * a), (-b - Math.Sqrt(discriminant)) / (2 * a));
        }

        public (Vector3, Vector3) ComputePointNormal(Vector3 origin, Vector3 viewCoord, double closest_t)
        {
            var p = origin + new Vector3((float)closest_t) * viewCoord;
            var n = p - Center;
            n /= new Vector3(n.Length());

            return (p, n);
        }
    }
}

