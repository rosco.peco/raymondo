﻿using System.Numerics;

using Raymondo;
using BigGustave;

var scene = new Scene
{
    Shapes = new List<IShape>
    {
        new Sphere
        {
            Center = new Vector3(0, -1, 3),
            Radius = 1,
            Color = new Vector3(255, 0, 0),     // Red
            Specular = 500,                     // Shiny
            Reflectivity = 0.2f,                // A bit reflective
        },
        new Sphere
        {
            Center = new Vector3(2, 0, 4),
            Radius = 1,
            Color = new Vector3(0, 0, 255),     // Blue
            Specular = 500,                     // Shiny
            Reflectivity = 0.3f,                // A bit more reflective
        },
        new Sphere
        {
            Center = new Vector3(-2, 0, 4),
            Radius = 1,
            Color = new Vector3(0, 255, 0),     // Green
            Specular = 10,                      // A bit shiny
            Reflectivity = 0.4f,                // Even more reflective
        },
        new Sphere
        {
            Center = new Vector3(0, -5001, 0),
            Radius = 5000,
            Color = new Vector3(255, 255, 0),   // Yellow
            Specular = 1000,                    // Very shiny!
            Reflectivity = 0.2f                 // Half reflective
        },
    },
    Lights = new List<ILight>
    {
        new AmbientLight
        {
            Intensity = 0.2f,
        },
        new PointLight
        {
            Intensity = 0.6f,
            Position = new Vector3(2, 1, 0),
        },
        new DirectionalLight
        {
            Intensity = 0.2f,
            Direction = new Vector3(1, 4, 4),
        },
    }
};

Console.WriteLine("There are " + scene.Shapes.Count + " sphere(s) and "
    + scene.Lights.Count + " light(s)");

var origin = new Vector3(0, 0, 0);
var background = new Vector3(255, 255, 255);
var raytracer = new RayTracer(1080, 1080, scene, origin, background, 2);

Console.WriteLine("About to start render...");
var data = raytracer.ThreadedRender();
File.WriteAllBytes("result.png", data);

Console.WriteLine("Done");