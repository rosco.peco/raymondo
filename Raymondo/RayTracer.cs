﻿using System.Numerics;

using BigGustave;

namespace Raymondo
{
    public class RayTracer
    {
        private const float VW = 1;
        private const float VH = 1;
        private const float VD = 1;
 
        private readonly PngBuilder bigGustave;
        private readonly int cw, ch;
        private readonly Scene scene;
        private readonly Vector3 origin;
        private readonly Vector3 background;
        private readonly int reflectionDepth;

        public RayTracer(int width, int height, Scene scene, Vector3 origin, Vector3 background, int reflectiondepth)
        {
            bigGustave = PngBuilder.Create(width, height, false);

            this.cw = width;
            this.ch = height;
            this.scene = scene;
            this.background = background;
            this.reflectionDepth = reflectiondepth;
        }

        public byte[] Render()
        {
            for (int x = -(cw/2); x < cw/2; x++)
            {
                for (int y = -(ch/2); y < ch/2; y++)
                {
                    var viewportVect = CanvasToViewport(x, y);
                    var color = TraceRay(origin, viewportVect, 1, double.PositiveInfinity, reflectionDepth);
                    bigGustave.SetPixel(color.ToPixel(), x + cw / 2, ch - (y + ch / 2) - 1);
                }
            }

            using (var mem = new MemoryStream())
            {
                bigGustave.Save(mem);
                return mem.ToArray();
            }
        }

        public byte[] ThreadedRender()
        {
            var completeList = new List<int>();
            pixels = new Pixel[ch, cw];

            int workerThreads, completionThreads;
            ThreadPool.GetMaxThreads(out workerThreads, out completionThreads);

            Console.WriteLine("Threaded render with max " + workerThreads + " worker thread(s)");
            for (int y = -(ch / 2); y < ch / 2; y++)
            {
                ThreadPool.QueueUserWorkItem(RenderLine, new RenderInfo
                {
                    LineNum = y,
                    CompleteList = completeList, 
                }) ;
            }

            Console.WriteLine("Queued work items...");

            var i = 100000;

            while (true) {
                lock (this)
                {
                    if (completeList.Count == ch)
                    {
                        break;
                    }
                }
                if (--i == 0)
                {
                    i = 100000;
                    lock (this)
                    {
                        //Console.WriteLine("Done " + completeList.Count + " of " + ch);
                    }
                }

            }

            for (int y = 0; y < ch; y++)
            {
                for (int x = 0; x < cw; x++)
                {
                    bigGustave.SetPixel(pixels[y,x], x, y);

                }
            }

            using (var mem = new MemoryStream())
            {
                bigGustave.Save(mem);
                return mem.ToArray();
            }
        }

        private Pixel[,] pixels;

        private struct RenderInfo
        {
            public int LineNum;
            public List<int> CompleteList;
        }

        private void RenderLine(Object? info)
        {
            if (info == null)
            {
                return;
            }

            var nonNullInfo = (RenderInfo?)info ?? new RenderInfo();

            for (int x = -(cw / 2); x < cw / 2; x++)
            {
                var viewportVect = CanvasToViewport(x, nonNullInfo.LineNum);
                var color = TraceRay(origin, viewportVect, 1, double.PositiveInfinity, reflectionDepth);
                lock (this)
                {
                    pixels[ch - (nonNullInfo.LineNum + ch / 2) - 1, x + cw / 2] = color.ToPixel();
                }
            }

            lock (this) {
                nonNullInfo.CompleteList.Add(nonNullInfo.LineNum);
            }
        }

        private Vector3 CanvasToViewport(int x, int y)
        {
            return new Vector3()
            {
                X = x * VW / cw,
                Y = y * VH / ch,
                Z = VD
            };
        }

        private Vector3 TraceRay(Vector3 origin, Vector3 viewCoord, double t_min, double t_max, int recursion_depth)
        {
            var (closest_shape, closest_t) = scene.ClosestIntersection(origin, viewCoord, t_min, t_max);

            if (closest_shape == null)
            {
                return this.background;
            }
            else
            {
                // Compute point and normalDir for lighting...
                var (p, n) = closest_shape.ComputePointNormal(origin, viewCoord, closest_t);
                var local_color = closest_shape.Color * ComputeLighting(p, n, viewCoord, closest_shape.Specular);

                // Reflections
                var r = closest_shape.Reflectivity;
                if (recursion_depth <= 0 || r <= 0)
                {
                    return local_color;
                }
                else
                {
                    var reflected = ReflectRay(-viewCoord, n);
                    var reflected_color = TraceRay(p, reflected, 0.001, double.PositiveInfinity, recursion_depth - 1);

                    return local_color * (1 - r) + (reflected_color) * r;
                }

            }
        }

        private static Vector3 ReflectRay(Vector3 r, Vector3 n)
        {
            return 2 * n * Vector3.Dot(n, r) - r;
        }

        private Vector3 ComputeLighting(Vector3 p, Vector3 n, Vector3 d, float specular)
        {
            var i = 0.0f;

            foreach (var light in scene.Lights)
            {
                i += light.Compute(scene, p, n, -d, specular);
            }

            return new Vector3(i);
        }
    }
}

