﻿using System.Numerics;

namespace Raymondo
{
    public interface ILight
    {
        public float Intensity { get; set; }

        public float Compute(Scene scene, Vector3 point, Vector3 normalDir, Vector3 view, float specular);
    }

    public class AmbientLight : ILight
    {
        public float Intensity { get; set; }

        public float Compute(Scene scene, Vector3 point, Vector3 normalDir, Vector3 view, float specular)
        {
            return Intensity;
        }
    }

    public abstract class RegularLight : ILight
    {
        public abstract float Intensity { get; set; }
        public abstract bool CastShadows { get; set; }

        protected RegularLight()
        {
            CastShadows = true;
        }

        public abstract float Compute(Scene scene, Vector3 point, Vector3 normalDir, Vector3 view, float specular);

        protected float ComputeSpecular(Vector3 normalDir, Vector3 view, Vector3 l, float specular)
        {
            if (specular != -1)
            {
                var r = 2 * normalDir * Vector3.Dot(normalDir, l) - l;
                var rdotv = Vector3.Dot(r, view);

                if (rdotv > 0)
                {
                    return (float)(Intensity * Math.Pow(rdotv / (r.Length() * view.Length()), specular));
                }
                else
                {
                    return 0.0f;
                }
            }
            else
            {
                return 0.0f;
            }
        }
    }

    public class PointLight : RegularLight
    {
        public override float Intensity { get; set; }
        public override bool CastShadows { get; set; }
        public Vector3 Position { get; set; }

        public override float Compute(Scene scene, Vector3 point, Vector3 normalDir, Vector3 view, float specular)
        {
            var i = 0.0f;
            Vector3 l = Position - point;

            /* Shadow check - t_max is 1 for point light */
            var (shadow_sphere, _) = scene.ClosestIntersection(point, l, 0.001, 1);
            if (CastShadows && shadow_sphere != null)
            {
                // In shadow for this light - skip it
                return 0.0f;
            }

            // Diffuse
            var ndotl = Vector3.Dot(normalDir, l);
            if (ndotl > 0)
            {
                i += Intensity * ndotl / (normalDir.Length() * l.Length());
            }

            // Specular
            return i + ComputeSpecular(normalDir, view, l, specular);
        }
    }

    public class DirectionalLight : RegularLight
    {
        public override float Intensity { get; set; }
        public override bool CastShadows { get; set; }
        public Vector3 Direction { get; set; }

        public override float Compute(Scene scene, Vector3 point, Vector3 normalDir, Vector3 view, float specular)
        {
            var i = 0.0f;

            var (shadow_sphere, _) = scene.ClosestIntersection(point, Direction, 0.001, double.PositiveInfinity);
            if (CastShadows && shadow_sphere != null)
            {
                // In shadow for this light - skip it
                return 0.0f;
            }

            // Diffuse
            var ndotl = Vector3.Dot(normalDir, Direction);
            if (ndotl > 0)
            {
                i += Intensity * ndotl / (normalDir.Length() * Direction.Length());
            }

            // Specular
            return i + ComputeSpecular(normalDir, view, Direction, specular);
        }
    }
}

