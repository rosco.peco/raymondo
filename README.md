﻿# Raymondo - A simple Raytracing / Rasterizing lib in C#

## What?

This is a simple raytracer & rasterizer based on the book "Computer Graphics
From Scratch - A Programmer's Introduction to 3D Rendering" by Gabriel Gambetta.

The reasons I wrote this are twofold:

* The book has been on my desk for months, waiting for me to do it!
* I wanted to polish my C# skills, which I haven't used for several years

It is not intended to exemplify best practices in either graphics programming
or C# / .NET development - it is purely a vehicle for learning and development.

## Who?

Copyright (c)2022 Ross Bamford; MIT License.